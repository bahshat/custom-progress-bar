----------------------Documentation---------------------
---------Author Rais (iOS & Corona Apps & Games Developer), Redbytes Software, Pune
---------Date 14-Feb-2014 
-----------Version 1.0
 


--This is an example how to implement custom progress bar

--require ("CustomProgressBar")--user need to require this module so that widget library get extended

--local widget = require( "widget" )
--
--local function myListener()
--    print("Progress has been completed 100%")
--end
--
--
--local progInfotable=
--{
--     duration=20000,
--     steps=30,
--     onComplete=myListener
--}
--
--local prgGrp=widget.newCustomProgress(progInfotable)
--prgGrp.x=500
--prgGrp.y=200
--prgGrp.xScale=2
--prgGrp.yScale=2


----------------------End Documentation---------------------


local widget = require( "widget" )
---widget library is extended using following function
-- newCustomProgress accepts single param as table (optional)
-- table.duration=number --total time span of the progress bar
-- table.step=number --total no of steps to complete 100%
-- table.onComplete=listener --on completion of 100% passed function we be called


function widget.newCustomProgress(progressbarPref)
    local progressbarGrp=display.newGroup()----main group in which display objects goes & finally this group is return to caller 
    
    local barImgIndicator=display.newImageRect(progressbarGrp,"ProgressIndicator.png", 236, 26)
    barImgIndicator.anchorX=1
    barImgIndicator.x=0
    barImgIndicator.y=0
    
    local barImgBG=display.newImageRect(progressbarGrp,"ProgressBG.png", 228, 26)
    barImgBG.anchorX=1
    barImgBG.x=barImgIndicator.x
    barImgBG.y=barImgIndicator.y
    -----------------------------
    
    -----If user doesn't pass parameter or pass but omits multiple or all parameter then the default values will be used
    local progressbarPref= progressbarPref or {}
    local steps=progressbarPref.steps or 10
    local timeSpan=progressbarPref.duration or 10000
    local listener=progressbarPref.onComplete or nil
    
    local stepWidth=barImgBG.width/steps --stepWidth is the size of the  barImgIndicator to be reduced each time makeProgress() called
    local interval=timeSpan/steps--interval is the time delay to call makeProgress() each time
    
    -----------------------------
    local function makeProgress()
        if barImgBG.width<0 or stepWidth>=barImgBG.width then
            barImgBG.width=0--if stepWidth>=barImgBG.width eg stepWidth = 50 & barImgBG.width =28 then 
            if listener~= nil then
                --if user passed a parameter in progressbarPref as listener then that should be called
                listener()
            end
            return true  ----barImgBG.width<0 then this makeProgress should not called again  
            
        end
        
        barImgBG.width=barImgBG.width-stepWidth ---reducing width
        
        timer.performWithDelay(interval, makeProgress)
    end
    
    makeProgress()--this recursive function decreases the width of the barImgIndicator each time called
    
    return progressbarGrp
end



