require ("CustomProgressBar")--user need to require this module so that widget library get extended

local widget = require( "widget" )

local function myListener()
    print("Progress has been completed 100%")
end


local progInfotable=
{
     duration=20000,
     steps=30,
     onComplete=myListener
}

local prgGrp=widget.newCustomProgress(progInfotable)
prgGrp.x=500
prgGrp.y=200
prgGrp.xScale=2
prgGrp.yScale=2






